package LoopAnswer;

/*
This loop prints out the multiplication table of 6.
I used a while loop here but you can use other types of loops.
*/

public class Main {
    public static void main(String[] args) {

        //While Loop
        int baseNumber = 6;
        int multiplier = 1;
        System.out.println("The multiplication table of 6");
        while(multiplier <= 10){
            System.out.println(baseNumber +" * "+ multiplier + " = " +baseNumber * multiplier);
            multiplier++;
        }
        System.out.println("\n\n");
        System.out.println("The multiplication table of 8");
        //For loop
        baseNumber = 8;
        for (multiplier = 1; multiplier <= 10; multiplier++){
            System.out.println(baseNumber +" * "+ multiplier + " = " +baseNumber * multiplier);
        }

    }
}