package LoopUserInputAnswer;

/*
Remove the /* and */ /* if you want to run the specific answers
*/

import java.util.Scanner;

public class Main {
    public static void main(String[] args)  {
        //User input multiplication table
        /*
        int baseNumber;
        int multiplier = 1;
        Scanner reader = new Scanner(System.in);
        System.out.println("What number do you want to get the multiplication table from?");
        baseNumber = reader.nextInt();

        while(multiplier <= 10){
            System.out.println(baseNumber +" * "+ multiplier + " = "+ baseNumber * multiplier);
            multiplier++;
        }
*/

        //Extra 1
        /*
        float baseNumber;
        int multiplier = 1;
        Scanner reader = new Scanner(System.in);
        System.out.println("What number do you want to get the multiplication table from?");
        baseNumber = reader.nextFloat();

        while(multiplier <= 10){
            System.out.println(baseNumber +" * "+ multiplier + " = "+ baseNumber * multiplier);
            multiplier++;
        }
*/

        //Extra 2
        /*
        float baseNumber = 0;
        int multiplier = 1;
        Scanner reader = new Scanner(System.in);
        Boolean correctInput = false;
        do {
            try {
                System.out.println("What number do you want to get the multiplication table from?");
                baseNumber = Float.parseFloat(reader.nextLine().replace(",", "."));
                correctInput = true;
            }
            catch (Exception e){
                System.out.println("Incorrect input, try again!");
            }
        } while (!correctInput);

        while(multiplier <= 10){
            System.out.println(baseNumber +" * "+ multiplier + " = "+ baseNumber * multiplier);

            multiplier ++;
        }
*/

    }
}
